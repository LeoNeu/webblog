package de.awacademy.unserWebblog.login;

import de.awacademy.unserWebblog.session.Session;
import de.awacademy.unserWebblog.session.SessionRepository;
import de.awacademy.unserWebblog.user.User;
import de.awacademy.unserWebblog.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Controller
public class LoginController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    @GetMapping("login")
    public String login(Model model) {
        model.addAttribute("login", new LoginDTO());
        return "login";
    }

    @PostMapping("login")
    public String loginSubmit(Model model, LoginDTO login, HttpServletResponse response) {
        Optional<User> optionalUser = userRepository.findFirstByNameAndPassword(login.getUsername(), login.getPassword());
        if (optionalUser.isPresent()) {
            Session s = new Session(optionalUser.get());
            sessionRepository.save(s);
            response.addCookie(new Cookie("sessionId", s.getId()));
            return "redirect:/blog";
        }
        model.addAttribute("login", login);
        return "login";
    }

    @PostMapping("logout")
    public String logout(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response) {
        sessionRepository.deleteById(sessionId);
        Cookie c = new Cookie("sessionId", "");
        c.setMaxAge(0);
        response.addCookie(c);
        return "redirect:/blog";
    }

    @PostMapping("logoutToHome")
    public String logoutToHome(@CookieValue(value = "sessionId", defaultValue = "") String sessionId, HttpServletResponse response) {
        sessionRepository.deleteById(sessionId);
        Cookie c = new Cookie("sessionId", "");
        c.setMaxAge(0);
        response.addCookie(c);
        return "redirect:/";
    }


}
