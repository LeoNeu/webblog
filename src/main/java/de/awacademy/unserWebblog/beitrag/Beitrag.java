package de.awacademy.unserWebblog.beitrag;

import de.awacademy.unserWebblog.kommentar.Kommentar;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String ueberschrift;

    @Lob
    private String text;

    private LocalDateTime erstellungsDatum;

    @OneToMany
    @JoinColumn(name = "beitrag_id")
    private List<Kommentar> kommentare = new ArrayList<Kommentar>();

    public Beitrag() {
    }

    ;

    public Beitrag(String ueberschrift, String text) {
        this.ueberschrift = ueberschrift;
        this.text = text;
        this.erstellungsDatum = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Beitrag{" +
                "id=" + id +
                ", ueberschrift='" + ueberschrift + '\'' +
                ", text='" + text + '\'' +
                ", erstellungsDatum=" + erstellungsDatum +
                '}';
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public long getId() {
        return id;
    }

    public String getUeberschrift() {
        return ueberschrift;
    }

    public void setUeberschrift(String ueberschrift) {
        this.ueberschrift = ueberschrift;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getErstellungsDatum() {
        return erstellungsDatum;
    }

    public void setErstellungsDatum(LocalDateTime erstellungsDatum) {
        this.erstellungsDatum = erstellungsDatum;
    }
}
