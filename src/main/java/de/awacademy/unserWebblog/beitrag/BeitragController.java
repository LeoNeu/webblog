package de.awacademy.unserWebblog.beitrag;

import de.awacademy.unserWebblog.kommentar.KommentarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class BeitragController {

    @Autowired
    BeitragRepository beitragRepository;

    @Autowired
    KommentarRepository kommentarRepository;


    //Beiträge anzeigen
    @GetMapping("/")
    public String showHomepage(Model model) {
        return "home";
    }

    @GetMapping("/blog")
    public String zeigeBeitraege(Model model) {
        model.addAttribute("beitraege", beitragRepository.findAllByOrderByErstellungsDatumDesc());//WICHTIG: für die Anzeige der Beitraege
        return "blog";
    }

    @PostMapping("/blog")
    public String showAllEntries(Model model) {

        //model.addAttribute("beitraege", beitragRepository.findAllByOrderByErstellungsDatumDesc());//allein NO
        model.addAttribute("kommentare", kommentarRepository.findAll());
        return "blog";
    }

    //Beitrag erstellen-create Beitrag
    @GetMapping("/beitrag")
    public String create(Model model) {
        model.addAttribute("beitrag", new BeitragDTO());
        return "createBeitrag";
    }

    @PostMapping("/beitrag")
    public String createBeitrag(@ModelAttribute("beitrag") @Valid BeitragDTO beitrag,
                                BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "createBeitrag";
        }

        Beitrag beitragEntity = new Beitrag(beitrag.getUeberschrift(), beitrag.getText());
        beitragRepository.save(beitragEntity);
        return "redirect:/blog";
    }

}

