package de.awacademy.unserWebblog.kommentar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface KommentarRepository extends JpaRepository<Kommentar, Long> {
}
