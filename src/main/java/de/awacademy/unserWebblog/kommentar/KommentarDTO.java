package de.awacademy.unserWebblog.kommentar;

import java.time.Instant;

public class KommentarDTO {

    private long kommentar_id;
    private String text;
    public Instant erstellungsdatum;
    private Long beitragId;

    public long getKommentar_id() {
        return kommentar_id;
    }

    public Long getBeitragId() {
        return beitragId;
    }

    public void setBeitragId(Long beitragId) {
        this.beitragId = beitragId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getErstellungsdatum() {
        return erstellungsdatum;
    }

    public void setErstellungsdatum(Instant erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }
}
