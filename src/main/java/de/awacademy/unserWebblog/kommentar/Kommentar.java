package de.awacademy.unserWebblog.kommentar;

import de.awacademy.unserWebblog.user.User;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDateTime;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long kommentar_id;

    @Lob
    private String text;

    public LocalDateTime erstellungsdatum;

    @ManyToOne
    private User user;

    public Kommentar() {
    }

    public Kommentar(String text, User user) {
        this.text = text;
        this.erstellungsdatum = LocalDateTime.now();
        this.user = user;
    }

    @Override
    public String toString() {
        return "Kommentar{" +
                "kommentar_id=" + kommentar_id +
                ", text='" + text + '\'' +
                ", erstellungsdatum=" + erstellungsdatum +
                '}';
    }

    public User getUser() {
        return user;
    }

    public long getKommentar_id() {
        return kommentar_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getErstellungsdatum() {
        return erstellungsdatum;
    }

    public void setErstellungsdatum(LocalDateTime erstellungsdatum) {
        this.erstellungsdatum = erstellungsdatum;
    }
}
