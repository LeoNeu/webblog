package de.awacademy.unserWebblog.kommentar;

import de.awacademy.unserWebblog.beitrag.Beitrag;
import de.awacademy.unserWebblog.beitrag.BeitragRepository;
import de.awacademy.unserWebblog.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class KommentarController {

    @Autowired
    KommentarRepository kommentarRepository;

    @Autowired
    BeitragRepository beitragRepository;

    //Kommentar erstellen
    @GetMapping("/kommentar/create/{beitragId}")
    public String createKommentar(Model model, @PathVariable(value = "beitragId") Long beitragId) {
        KommentarDTO newKDTO = new KommentarDTO();
        newKDTO.setBeitragId(beitragId);
        model.addAttribute("kommentar", newKDTO);
        return "createComment";
    }


    @PostMapping("/kommentar/create")
    public String createComment(@ModelAttribute("kommentar") @Valid KommentarDTO kommentar,
                                @ModelAttribute("currentUser") User currentUser, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "createComment";
        }

        Kommentar kommentarEntity = new Kommentar(kommentar.getText(), currentUser);
        kommentarRepository.save(kommentarEntity);
        Optional<Beitrag> beitrag = beitragRepository.findById(kommentar.getBeitragId());
        beitrag.get().getKommentare().add(kommentarEntity);
        beitragRepository.save(beitrag.get());


        return "redirect:/blog";
    }

    @PostMapping("/deleteKommentar")
    public String deleteCommentAsAdmin(@RequestParam("kommentarId") long kommentarId,
                                       @ModelAttribute("currentUser") User currentUser) {
        Optional<Kommentar> kommentarToDelete = kommentarRepository.findById(kommentarId);
        if (kommentarToDelete.isPresent() && (currentUser.isAdministrator() ||
                currentUser == kommentarToDelete.get().getUser())) {
            kommentarRepository.delete(kommentarToDelete.get());
        }
        return "redirect:/blog";
    }

}
