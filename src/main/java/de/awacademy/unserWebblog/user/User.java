package de.awacademy.unserWebblog.user;

import de.awacademy.unserWebblog.kommentar.Kommentar;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String name;
    private String password;
    private boolean administrator;

    @OneToMany(mappedBy = "user")
    private List<Kommentar> kommentare;

    public User() {
    }

    public User(String name, String password, boolean administrator) {
        this.name = name;
        this.password = password;
        this.administrator = administrator;
    }

    public String getName() {
        return name;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public long getId() {
        return id;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }
}
