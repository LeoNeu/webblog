package de.awacademy.unserWebblog.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findFirstByNameAndPassword(String name, String password);

    //Für Signup
    boolean existsByName(String name);
}
