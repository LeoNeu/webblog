package de.awacademy.unserWebblog.user;

import javax.validation.constraints.NotEmpty;

public class UserDTO {

    @NotEmpty
    private String name;
    private String password;
    private boolean administrator;
    private long user_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAdministrator() {
        return administrator;
    }

    public void setAdministrator(boolean administrator) {
        this.administrator = administrator;
    }

    public long getUser_id() {
        return user_id;
    }
}
